<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>it.cnr.iit</groupId>
	<artifactId>bundle-manager-v2</artifactId>
	<version>0.0.8-SNAPSHOT</version>
	<packaging>war</packaging>

	<properties>
		<springfox.version>2.9.2</springfox.version>
		<swagger.version>1.5.22</swagger.version>
		<swagger2markup.version>1.3.3</swagger2markup.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.2.1.RELEASE</version>
	</parent>

	<dependencies>
	
		<dependency>
    		<groupId>org.springframework</groupId>
    		<artifactId>spring-mock</artifactId>
    		<version>2.0.8</version>
		</dependency>
	
		<dependency>
			<groupId>com.j256.ormlite</groupId>
			<artifactId>ormlite-core</artifactId>
			<version>4.48</version>
		</dependency>
		
		<dependency>
			<groupId>com.j256.ormlite</groupId>
			<artifactId>ormlite-jdbc</artifactId>
			<version>4.48</version>
		</dependency>
		
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
		</dependency>
		
		<dependency>
  			<groupId>it.cnr.iit</groupId>
  			<artifactId>vault-api</artifactId>
  			<version>0.0.2-SNAPSHOT</version>
		</dependency>
	
		<dependency>
			<groupId>it.cnr.iit</groupId>
			<artifactId>c3isp-common</artifactId>
			<version>0.1.6-SNAPSHOT</version>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.cloud</groupId>
        	<artifactId>spring-cloud-starter-config</artifactId>
        	<version>2.2.1.RELEASE</version>
       	</dependency>
		
		<dependency>
    		<groupId>org.apache.commons</groupId>
    		<artifactId>commons-lang3</artifactId>
		</dependency>

		<!-- https://mvnrepository.com/artifact/commons-io/commons-io -->
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.6</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/com.att.research.xacml/xacml -->
		<dependency>
			<groupId>com.att.research.xacml</groupId>
			<artifactId>xacml</artifactId>
			<version>2.0.1</version>
		</dependency>

		<!-- spring boot dependencies plus tomcat deployed -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<optional>true</optional>
		</dependency>

		<!-- add log4j2 instead of default logback -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-log4j2</artifactId>
		</dependency>

		<!-- SpringFox dependencies -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>3.0.0</version>
		</dependency>

		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>3.0.0</version>
		</dependency>

		<!-- to generate API documentation -->
		<dependency>
			<groupId>io.github.swagger2markup</groupId>
			<artifactId>swagger2markup</artifactId>
			<version>${swagger2markup.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- swagger core dependencies -->
		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-annotations</artifactId>
			<version>${swagger.version}</version>
		</dependency>

		<dependency>
			<groupId>io.swagger</groupId>
			<artifactId>swagger-core</artifactId>
			<version>${swagger.version}</version>
		</dependency>

		<!-- Libraries for tests -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Libraries for security -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- https://mvnrepository.com/artifact/javax.servlet/servlet-api -->
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>servlet-api</artifactId>
			<version>2.4</version>
			<scope>provided</scope>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/javax.xml.bind/jaxb-api -->
		<dependency>
		    <groupId>javax.xml.bind</groupId>
		    <artifactId>jaxb-api</artifactId>
		</dependency>
		

		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
		</dependency>
	</dependencies>

	<build>
		<finalName>bundle-manager</finalName>
		<plugins>
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<executions>
					<execution>
						<id>build-info</id>
						<goals>
							<goal>build-info</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<artifactId>maven-war-plugin</artifactId>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
			</plugin>

			<!-- FindBugs and FindSecurityBugs plugins -->
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<version>3.0.2</version>
				<configuration>
					<effort>Max</effort>
					<threshold>Low</threshold>
					<failOnError>true</failOnError>
					<plugins>
						<plugin>
							<groupId>com.h3xstream.findsecbugs</groupId>
							<artifactId>findsecbugs-plugin</artifactId>
							<version>1.7.1</version>
						</plugin>
					</plugins>
				</configuration>
			</plugin>

			<!-- JaCoCo plugin bound to test phase -->
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.9</version>
				<executions>
					<execution>
						<id>pre-unit-test</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- OWASP Dependency-Check plugin -->
			<plugin>
				<groupId>org.owasp</groupId>
				<artifactId>dependency-check-maven</artifactId>
				<version>3.0.2</version>
				<configuration>
					<format>XML</format>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.asciidoctor</groupId>
				<artifactId>asciidoctor-maven-plugin</artifactId>
				<version>1.5.6</version>
				<!-- Include Asciidoctor PDF for pdf generation -->
				<!-- See: https://github.com/asciidoctor/asciidoctorj/issues/416 -->
				<dependencies>
					<dependency>
						<groupId>org.asciidoctor</groupId>
						<artifactId>asciidoctorj-pdf</artifactId>
						<version>1.5.0-alpha.16</version>
					</dependency>
					<dependency>
						<groupId>org.jruby</groupId>
						<artifactId>jruby-complete</artifactId>
						<version>1.7.26</version>
					</dependency>
				</dependencies>
				<configuration>
					<!-- <sourceDirectory>${asciidoctor.input.directory}</sourceDirectory> -->
					<sourceDirectory>${project.basedir}/target/docs/asciidoc</sourceDirectory>
					<!-- <sourceDocumentName>index.adoc</sourceDocumentName> -->
					<attributes>
						<doctype>book</doctype>
						<toc>left</toc>
						<toclevels>3</toclevels>
						<numbered></numbered>
						<hardbreaks></hardbreaks>
						<sectlinks></sectlinks>
						<sectanchors></sectanchors>
						<!-- <generated>${generated.asciidoc.directory}</generated> -->
					</attributes>
				</configuration>
				<executions>
					<execution>
						<id>output-html</id>
						<phase>test</phase>
						<goals>
							<goal>process-asciidoc</goal>
						</goals>
						<configuration>
							<!-- backend values = pdf | html -->
							<backend>html</backend>
							<!-- <outputDirectory>${asciidoctor.pdf.output.directory}</outputDirectory> -->
							<sourceHighlighter>coderay</sourceHighlighter>
						</configuration>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>

	<!-- to enable download from internal Nexus repositories -->
	<repositories>
		<repository>
       		<id>nexus.snapshots</id>
       		<url>http://nexusc3isp.iit.cnr.it:8081/repository/maven-snapshots/</url>
       		<releases>
            	<enabled>false</enabled>
       		</releases>
       		<snapshots>
            	<enabled>true</enabled>
       		</snapshots>
		</repository>
		<repository>
       		<id>nexus.releases</id>
       		<url>http://nexusc3isp.iit.cnr.it:8081/repository/maven-releases/</url>
       		<releases>
            	<enabled>true</enabled>
       		</releases>
       		<snapshots>
            	<enabled>false</enabled>
       		</snapshots>
		</repository>
		<repository>
			<!-- Used for dependencies due to doc generation -->
			<id>jcenter-releases</id>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<name>jcenter</name>
			<url>https://jcenter.bintray.com</url>
		</repository>
		<repository>
			<id>central</id>
			<name>Maven Repository Switchboard</name>
			<url>https://repo1.maven.org/maven2</url>
		</repository>
		<repository>
			<id>wso2-maven2-repository</id>
			<url>http://dist.wso2.org/maven2</url>
		</repository>
	</repositories>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>2.17</version>
				<reportSets>
					<reportSet>
						<reports>
							<report>checkstyle</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
		</plugins>
	</reporting>

</project>
