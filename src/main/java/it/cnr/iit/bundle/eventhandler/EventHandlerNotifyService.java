package it.cnr.iit.bundle.eventhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import it.cnr.iit.bundle.utils.EventTypes;
import it.cnr.iit.common.eventhandler.events.SubscribeEvent;

public class EventHandlerNotifyService {
	private static final Logger log = LoggerFactory.getLogger(EventHandlerNotifyService.class);

	private String callSubcribeEventHandler;

	private String callNotifyBundleManager;

	public EventHandlerNotifyService(String callSubcribeEventHandler, String callNotifyBundleManager) {
		this.callNotifyBundleManager = callNotifyBundleManager;
		this.callSubcribeEventHandler = callSubcribeEventHandler;
	}

	public void subscribeEventHandler() {

		String[] arEvents = new String[] { EventTypes.BUNDLE_MANAGER_CREATE, EventTypes.BUNDLE_MANAGER_DELETE,
				EventTypes.BUNDLE_MANAGER_READ };

		RestTemplate restTemplate = new RestTemplate();
		for (String event : arEvents) {
			SubscribeEvent objRequest = new SubscribeEvent(event, callNotifyBundleManager);

			log.info("[BUNDLE-MANAGER] Start to subscribe to Event Handler " + callSubcribeEventHandler + ": Event ["
					+ event + "] : " + objRequest.toString());
			try {
				String res = restTemplate.postForObject(callSubcribeEventHandler, objRequest, String.class);
				log.info("[BUNDLE-MANAGER] response from '" + callSubcribeEventHandler + "' : " + res);
			} catch (HttpClientErrorException e) {
				log.info("[BUNDLE-MANAGER] HttpClientErrorException SubcribeEventHandler to Event Handler \n "
						+ e.toString());
			} catch (RestClientException e) {
				log.info("[BUNDLE-MANAGER] Error executing request SubcribeEventHandler to Event Handler \n "
						+ e.toString());
				log.info("[BUNDLE-MANAGER] sending request via : " + callSubcribeEventHandler);
			} catch (Exception e) {
				log.info("[BUNDLE-MANAGER] Error Exception request SubcribeEventHandler to Event Handler \n "
						+ e.toString());
				log.info("[BUNDLE-MANAGER] sending request via : " + callSubcribeEventHandler);
			}

		}
	}
}
