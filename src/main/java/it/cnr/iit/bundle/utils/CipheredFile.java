package it.cnr.iit.bundle.utils;

public class CipheredFile {

	private String path;
	private String cipheredKey;

	public CipheredFile(String path, String key) {
		this.path = path;
		this.cipheredKey = key;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCipheredKey() {
		return cipheredKey;
	}

	public void setCipheredKey(String cipheredKey) {
		this.cipheredKey = cipheredKey;
	}

}
