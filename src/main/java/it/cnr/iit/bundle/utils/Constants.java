package it.cnr.iit.bundle.utils;

public class Constants {

	public static final String DSA_ID_PARAM = "dsaId";
	public static final String DPO_ID_PARAM = "dposId";
	public static final String REQUEST_ID_PARAM = "requestID";
	public static final String TMP_SYMMETRIC_KEY_PARAM = "tmpSymmetricKey";
	public static final String METADATA_PARAM = "metadataFile";
	public static final String CONTENT_PARAM = "ctiFile";

	public static final String POLICY_FILE_NAME = "DSAFile";
	public static final String DPO_METADATA_FILE_NAME = "DPOMetadata";
	public static final String CONTENT_FILE_NAME = "CTIFile";
	public static final String HASHCODE_FILE_NAME = "HashCode";

}
