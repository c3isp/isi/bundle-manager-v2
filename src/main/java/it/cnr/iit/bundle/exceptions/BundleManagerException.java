package it.cnr.iit.bundle.exceptions;

public class BundleManagerException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public BundleManagerException(String message) {
		super(message);
	}

}
