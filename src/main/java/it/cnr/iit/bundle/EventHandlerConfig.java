package it.cnr.iit.bundle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

import it.cnr.iit.bundle.eventhandler.EventHandlerNotifyService;

@Configuration
@EnableScheduling
public class EventHandlerConfig {

	private static final Logger log = LoggerFactory.getLogger(EventHandlerConfig.class);

////	@Value("${rest.endpoint.url.callRegistedEventHandler}")
//	private String callSubcribeEventHandler;
//
////	@Value("${rest.endpoint.url.callNotifyBundleManager}")
//	private String callNotifyBundleManager;

	@Value("${rest.endpoint.url.eventHandlerSubscribe}")
	private String eventHandlerSubscribe;

//	@Value("${rest.endpoint.url.eventHandlerNotify}")
//	private String eventHandlerNotify;

	@Value("${rest.endpoint.url.callNotifyBundleManager}")
	private String bundleManagerNotifyEvent;

//	@Autowired
//	public EventHandlerConfig(@Value("${rest.endpoint.url.eventHandlerSubscribe}") String registerLink,
//			@Value("${rest.endpoint.url.eventHandlerNotify}") String notifyLink) {
//		this.callSubcribeEventHandler = registerLink;
//		this.callNotifyBundleManager = notifyLink;
//
//		log.info("Initialized Instance EventHandlerConfig with " + callSubcribeEventHandler + " and "
//				+ callNotifyBundleManager);
//	}

	@Bean
	public TaskScheduler taskScheduler() {
		return new ConcurrentTaskScheduler(); // single threaded by default
	}

	@Scheduled(fixedDelay = 30000, initialDelay = 10000)
	public void scheduleConnectingEventHandlerTask() {
		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		String strDate = dateFormat.format(date);
		new EventHandlerNotifyService(eventHandlerSubscribe, bundleManagerNotifyEvent).subscribeEventHandler();
	}

	@PreDestroy
	public void stop() {

	}

}
