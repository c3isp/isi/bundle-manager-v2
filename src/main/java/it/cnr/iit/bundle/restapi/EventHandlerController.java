package it.cnr.iit.bundle.restapi;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.cnr.iit.bundle.exceptions.BundleManagerException;
import it.cnr.iit.bundle.impl.BundleManagerService;
import it.cnr.iit.bundle.utils.EventTypes;
import it.cnr.iit.common.eventhandler.events.RequestEvent;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;

@ApiModel(value = "EventHandlerController", description = "Bundle Manager receiver for Event Handler events")
@RestController
@RequestMapping("/v1")
public class EventHandlerController {

	private static final Logger log = Logger.getLogger(EventHandlerController.class.getName());

	@Value(value = "${security.user.name}")
	private String restUser;
	@Value(value = "${security.user.password}")
	private String restPassword;

	@Autowired
	BundleManagerService bundleManagerService;

	@ApiResponses(value = { @ApiResponse(code = 412, message = "Client Error"),
			@ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Request accepted") })
	@ApiOperation(httpMethod = "POST", value = "Receives event notifications from the EventHandler")
	@PostMapping(value = { "/notifyEvent" })
	public ResponseEntity<ResponseEvent> receiveEvent(@RequestBody(required = true) RequestEvent event) {
		try {
			switch (event.getEventType()) {

			case (EventTypes.BUNDLE_MANAGER_CREATE):
				return new ResponseEntity<ResponseEvent>(bundleManagerService.bundleManagerCreate(event),
						HttpStatus.OK);

			case (EventTypes.BUNDLE_MANAGER_DELETE):
				return new ResponseEntity<ResponseEvent>(bundleManagerService.bundleManagerDelete(event),
						HttpStatus.OK);

			case (EventTypes.BUNDLE_MANAGER_READ):
				return new ResponseEntity<ResponseEvent>(bundleManagerService.bundleManagerRead(event), HttpStatus.OK);

			default:
				throw new BundleManagerException("Unknown event received");
			}

		} catch (Exception e) {
			log.severe(e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
