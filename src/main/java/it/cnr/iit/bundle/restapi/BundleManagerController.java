package it.cnr.iit.bundle.restapi;

import java.util.logging.Logger;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "BundleManager", description = "Bundle Manager")
@RestController
@RequestMapping("/v1")
public class BundleManagerController {

	private static final Logger log = Logger.getLogger(BundleManagerController.class.getName());

}
