package it.cnr.iit.bundle.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.UUID;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.bundle.db.DpoCipheredKey;
import it.cnr.iit.bundle.db.DpoCipheredKeyStorage;
import it.cnr.iit.bundle.exceptions.BundleManagerException;
import it.cnr.iit.bundle.utils.CipheredFile;
import it.cnr.iit.bundle.utils.Constants;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerCreateResponseEvent;
import it.cnr.iit.common.eventhandler.events.bundlemanager.BundleManagerReadResponseEvent;
import it.cnr.iit.common.reject.Reject;
import it.cnr.iit.common.types.DPO;
import it.cnr.iit.vault.operations.CipherOperations;
import it.cnr.iit.vault.types.VaultCiphertext;
import it.cnr.iit.vault.types.VaultDatakey;
import it.cnr.iit.vault.types.VaultResponse;
import it.cnr.iit.vault.types.VaultWrapper;

@Component
public class BundleManagerHelper {

	private static final Logger log = Logger.getLogger(BundleManagerService.class.getName());

	@Value(value = "${security.user.name}")
	private String restUser;
	@Value(value = "${security.user.password}")
	private String restPassword;

	@Value("${rest.endpoint.url.getPolicy}")
	private String dsaUri;

	@Value("${rest.endpoint.url.createDPO}")
	private String dposUriCreate;

	@Value("${rest.endpoint.url.readDPO}")
	private String dposUriRead;

	@Value("${rest.endpoint.url.eventHandlerNotify}")
	private String eventHandlerNotify;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private VaultWrapper vault;

	@Autowired
	private DpoCipheredKeyStorage dpoCipheredKeyStorage;

	public DPO decryptDPO(DPO dpo, String dpoId, VaultDatakey key) throws Exception {
		DPO res = new DPO();
		String filename = "/tmp/" + UUID.randomUUID().toString() + ".enc";
		File ctiFile = new File(filename);

		FileUtils.writeByteArrayToFile(ctiFile, dpo.getCti());

		// TODO: here's the bug
		String plainContentUri = vault.getCipherOperations().performOperation(key, ctiFile.getAbsolutePath(),
				CipherOperations.DECRYPT_MODE);
		byte[] plainContent = Files.readAllBytes(Paths.get(plainContentUri));

		res.setCti(plainContent);
		res.setDsa(dpo.getDsa());
		res.setHash(dpo.getHash());
		res.setMetadata(dpo.getMetadata());

		ctiFile.delete();

		return res;
	}

	public VaultDatakey getKeyFromDB(String dpoId) throws Exception {
		DpoCipheredKey key = dpoCipheredKeyStorage.getForDpoId(dpoId);

		VaultCiphertext cipherKey = new VaultCiphertext();

		cipherKey.setCiphertext(key.getCipheredKey());

		VaultResponse response = new ObjectMapper().readValue(vault.getHttpUtils().decryptDatakey(cipherKey),
				VaultResponse.class);

		return response.getDatakey();
	}

	public void createDPO(String policy, String contentFilePath, String metadata) throws IOException {
		// TODO: policy, header and
//		 content must be passed as MultipartFile to DPOS-api

		File contentFile = new File(contentFilePath);

		File policyFile = File.createTempFile(Constants.POLICY_FILE_NAME, ".xml");
		BufferedWriter writer = new BufferedWriter(new FileWriter(policyFile));
		writer.write(policy);
		writer.close();

		File metadataFile = File.createTempFile(Constants.DPO_METADATA_FILE_NAME, null);
		writer = new BufferedWriter(new FileWriter(metadataFile));
		writer.write(metadata);
		writer.close();

		File hashFile = File.createTempFile(Constants.HASHCODE_FILE_NAME, null);

		URI createDPO = UriComponentsBuilder.fromUriString(dposUriCreate).build().toUri();

		HttpHeaders headers = new HttpHeaders();
		String credentials = Base64.getEncoder().encodeToString((restUser + ":" + restPassword).getBytes());
		headers.add("Authorization", "Basic " + credentials);

		MultiValueMap<String, Object> mvm = new LinkedMultiValueMap<String, Object>();
		mvm.add(Constants.POLICY_FILE_NAME, new FileSystemResource(policyFile));
		mvm.add(Constants.DPO_METADATA_FILE_NAME, new FileSystemResource(metadataFile));
		mvm.add(Constants.CONTENT_FILE_NAME, new FileSystemResource(contentFile));
		mvm.add(Constants.HASHCODE_FILE_NAME, new FileSystemResource(hashFile));

		HttpEntity request = new HttpEntity(headers, mvm);
		ResponseEntity<String> response = this.restTemplate.postForEntity(createDPO, mvm, String.class);
	}

	public void storePairInDatabase(String dpoId, String cipheredKey) throws Exception {

		DpoCipheredKey dpoKey = new DpoCipheredKey();
		dpoKey.setDpoId(dpoId);
		dpoKey.setCipheredKey(cipheredKey);
		dpoCipheredKeyStorage.createOrUpdate(dpoKey);

	}

	public String getPolicy(String dsaId) throws BundleManagerException {
		String uri = dsaUri + dsaId;

		ResponseEntity<String> response = this.restTemplate.getForEntity(uri, String.class);
		Reject.ifNull(response);

		if (response.getBody() != null) {
			return response.getBody();
		} else {
			throw new BundleManagerException(response.toString());
		}
	}

	public CipheredFile getChipheredFileUri(String filePath, String tmpKey) throws BundleManagerException {
		try {
			// TODO: NEED TO DECRYPT the file with the tmp key and crypt it with a new key
			// obtained
			// from vault
			VaultDatakey key = new VaultDatakey();
			key.setPlaintext(tmpKey);

			filePath = vault.getCipherOperations().performOperation(key, filePath, CipherOperations.DECRYPT_MODE);

			// new encryption

			String response = vault.getHttpUtils().getCompleteDataKeyResponse();
			VaultResponse vaultResponse = new ObjectMapper().readValue(response, VaultResponse.class);
			VaultDatakey datakey = vaultResponse.getDatakey();

			return new CipheredFile(
					vault.getCipherOperations().performOperation(datakey, filePath, CipherOperations.ENCRYPT_MODE),
					datakey.getCiphertext());

		} catch (Exception e) {
			log.severe("error in getting chiphered file");
			e.printStackTrace();
			throw new BundleManagerException("error in getting chiphered file");
		}
	}

	public void sendBundleManagerCreateResponse(String dpoId, String requestID) {
		BundleManagerCreateResponseEvent bmcr = new BundleManagerCreateResponseEvent(dpoId, requestID);
		ResponseEvent responseEvent = restTemplate.postForObject(eventHandlerNotify, bmcr, ResponseEvent.class);
		if (!responseEvent.isSuccess()) {
			log.severe("error sending response event to Event Handler");
		}
	}

	public void sendBundleManagerReadResponse(String contentUri, String policyUri, String metadataUri,
			String requestID/*
							 * , VaultDatakey key
							 */) throws Exception {
		BundleManagerReadResponseEvent bmrr = new BundleManagerReadResponseEvent(contentUri, policyUri, metadataUri,
				"ClearFormat", requestID);
//		bmrr.getAdditionalProperties().put(BundleManagerEventConstants.TMP_SYMMETRIC_KEY,
//				new ObjectMapper().writeValueAsString(key));
		ResponseEvent responseEvent = restTemplate.postForObject(eventHandlerNotify, bmrr, ResponseEvent.class);
		if (!responseEvent.isSuccess()) {
			log.severe("error sending response event to Event Handler");
		}
	}
}
