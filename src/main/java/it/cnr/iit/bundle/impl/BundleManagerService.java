package it.cnr.iit.bundle.impl;

import java.io.File;
import java.net.URI;
import java.util.UUID;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.bundle.utils.CipheredFile;
import it.cnr.iit.bundle.utils.Constants;
import it.cnr.iit.common.eventhandler.events.RequestEvent;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.types.DPO;
import it.cnr.iit.common.types.Metadata;

@Component
public class BundleManagerService {

	@Value("${rest.endpoint.url.readDPO}")
	private String dposUriRead;

	@Value("${rest.endpoint.url.deleteDPO}")
	private String dposUriDelete;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private BundleManagerHelper helper;

	private static final Logger log = Logger.getLogger(BundleManagerService.class.getName());

	public ResponseEvent bundleManagerCreate(RequestEvent event) throws Exception {
		String policy = helper.getPolicy(event.getAdditionalProperties().get(Constants.DSA_ID_PARAM));
		String header = event.getAdditionalProperties().get(Constants.METADATA_PARAM);

		CipheredFile cipheredFile = helper.getChipheredFileUri(
				event.getAdditionalProperties().get(Constants.CONTENT_PARAM),
				event.getAdditionalProperties().get(Constants.TMP_SYMMETRIC_KEY_PARAM));
		String content = cipheredFile.getPath();
		Metadata metadataObj = new ObjectMapper().readValue(header, Metadata.class);

		// this is the dpoId that will be used for this DPO
		String dpoId = metadataObj.getId();

		helper.createDPO(policy, content, header);
		helper.storePairInDatabase(dpoId, cipheredFile.getCipheredKey());
		helper.sendBundleManagerCreateResponse(dpoId, event.getAdditionalProperties().get(Constants.REQUEST_ID_PARAM));

		return new ResponseEvent(true);
	}

	public ResponseEvent bundleManagerRead(RequestEvent event) throws Exception {
		String dpoId = event.getAdditionalProperties().get(Constants.DPO_ID_PARAM);
		URI readUri = UriComponentsBuilder.fromUriString(dposUriRead).pathSegment(dpoId).build().toUri();

		ResponseEntity<DPO> response = restTemplate.exchange(readUri, HttpMethod.GET, null, DPO.class);

		DPO encryptedDPO = response.getBody();
		DPO decryptedDPO = helper.decryptDPO(encryptedDPO, dpoId, helper.getKeyFromDB(dpoId));

		String fileName = "/tmp/" + UUID.randomUUID().toString();

		File contentFile = new File(fileName + ".payload");
		FileUtils.writeByteArrayToFile(contentFile, decryptedDPO.getCti());

		File policyFile = new File(fileName + ".dsa");
		FileUtils.writeByteArrayToFile(policyFile, decryptedDPO.getDsa());

		File metadataFile = new File(fileName + ".head");
		FileUtils.writeByteArrayToFile(metadataFile, decryptedDPO.getMetadata());

//		File contentFile = new File(fileName + ".payload");
//		FileUtils.writeByteArrayToFile(contentFile, encryptedDPO.getCti());
//
//		File policyFile = new File(fileName + ".dsa");
//		FileUtils.writeByteArrayToFile(policyFile, encryptedDPO.getDsa());
//
//		File metadataFile = new File(fileName + ".head");
//		FileUtils.writeByteArrayToFile(metadataFile, encryptedDPO.getMetadata());

		// here only contentFile is base64 encoded

		helper.sendBundleManagerReadResponse(contentFile.getPath(), policyFile.getPath(), metadataFile.getPath(),
				event.getAdditionalProperties().get(Constants.REQUEST_ID_PARAM)/* , helper.getKeyFromDB(dpoId) */);
		return new ResponseEvent(true);
	}

	public ResponseEvent bundleManagerDelete(RequestEvent event) throws Exception {
		String dpoId = event.getAdditionalProperties().get(Constants.DPO_ID_PARAM);
		URI deleteUri = UriComponentsBuilder.fromUriString(dposUriDelete).pathSegment(dpoId).build().toUri();

		ResponseEntity<String> response = restTemplate.exchange(deleteUri, HttpMethod.DELETE, null, String.class);
		return new ResponseEvent(true);
	}
}
