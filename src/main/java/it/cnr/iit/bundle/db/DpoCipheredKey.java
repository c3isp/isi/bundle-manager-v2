package it.cnr.iit.bundle.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "dpo_keys")
public class DpoCipheredKey {
	public static final String DPO_ID_FIELD = "dpo_id";
	public static final String CIPHERED_KEY_FIELD = "ciphered_key";

	@DatabaseField(id = true, columnName = DPO_ID_FIELD)
	private String dpoId;

	@DatabaseField(columnName = CIPHERED_KEY_FIELD)
	private String cipheredKey;

	public String getDpoId() {
		return dpoId;
	}

	public void setDpoId(String dpoId) {
		this.dpoId = dpoId;
	}

	public String getCipheredKey() {
		return cipheredKey;
	}

	public void setCipheredKey(String cipheredKey) {
		this.cipheredKey = cipheredKey;
	}

}
