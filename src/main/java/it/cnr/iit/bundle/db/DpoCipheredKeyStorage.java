package it.cnr.iit.bundle.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import it.cnr.iit.common.reject.Reject;

@Component
public class DpoCipheredKeyStorage {

	private Logger log = Logger.getLogger(DpoCipheredKeyStorage.class.getName());

	private ConnectionSource connection = null;
	private Dao<DpoCipheredKey, Integer> dao = null;

	@Value("${db.connection:jdbc:sqlite::memory:}")
	private String dbConnection;

	@PostConstruct
	public void init() {
		refresh();
	}

	private synchronized void refresh() {
		if (connection == null || !connection.isOpen()) {
			try {
				connection = new JdbcPooledConnectionSource(dbConnection);
				dao = DaoManager.createDao(connection, DpoCipheredKey.class);
				TableUtils.createTableIfNotExists(connection, DpoCipheredKey.class);
				log.info(() -> this.getClass().getSimpleName() + " database connection established.");
			} catch (SQLException e) {
				log.severe(() -> e.getClass().getSimpleName() + " while refreshing connection : " + e.getMessage());
			}
		}
	}

	public boolean createOrUpdate(DpoCipheredKey dpoInfo) {
		try {
			refresh();
			dao.createOrUpdate(dpoInfo);
			return true;
		} catch (SQLException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			return false;
		}
	}

	public boolean delete(DpoCipheredKey dpoInfo) {
		try {
			refresh();
			dao.delete(dpoInfo);
			return true;
		} catch (SQLException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			return false;
		}
	}

	public DpoCipheredKey getForDpoId(String dpoId) {
		return getForField(DpoCipheredKey.DPO_ID_FIELD, dpoId);
	}

	private DpoCipheredKey getForField(String column, String value) {
		List<DpoCipheredKey> tables = getForFields(column, value);
		if (tables == null || tables.size() > 1) {
			throw new IllegalStateException("Same field used multiple times");
		}
		return tables.stream().findFirst().orElse(null);
	}

	public List<DpoCipheredKey> getForFields(String column, String value) {
		Reject.ifBlank(column);
		Reject.ifBlank(value);
		try {
			refresh();
			QueryBuilder<DpoCipheredKey, Integer> qbAttributes = dao.queryBuilder();
			return qbAttributes.where().eq(column, value).query();
		} catch (Exception e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + column + " : " + value + " , " + e.getMessage());
		}
		return new ArrayList<>();
	}

}
