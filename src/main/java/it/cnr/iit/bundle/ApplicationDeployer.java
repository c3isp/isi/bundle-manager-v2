/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.bundle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * This class is the main spring boot entry, it implements the initializer class
 * and provides a main method that starts the application
 */
@SpringBootApplication
@ComponentScan("it.cnr.iit")
//@EnableSwagger2
public class ApplicationDeployer extends SpringBootServletInitializer {

//	@Value("${security.activation.status:false}")
//	private boolean securityActivationStatus;
//
//	/**
//	 * Spring boot method for configuring the current application, right now it
//	 * automatically scans for interfaces annotated via spring boot methods in all
//	 * sub classes
//	 */
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		return application.sources(ApplicationDeployer.class);
//	}
//
//	/**
//	 * Docket is a SwaggerUI configuration component, in particular specifies to use
//	 * the V2.0 (SWAGGER_2) of swagger generated interfaces it also tells to include
//	 * only paths that are under /v1/. If other rest interfaces are added with
//	 * different base path, they won't be included this path selector can be removed
//	 * if all interfaces should be documented.
//	 */
//	@Bean
//	public Docket documentation() {
//		Docket docket = new Docket(DocumentationType.SWAGGER_2);
//		docket.apiInfo(metadata());
//		if (!securityActivationStatus) {
//			return docket.select().paths(PathSelectors.regex("/v1/.*"))
//					.apis(RequestHandlerSelectors.basePackage("fr.cea.bundle.restapi")).build()
//					.directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
//					.directModelSubstitute(java.time.OffsetDateTime.class, java.util.Date.class);
//		} else {
//			return docket
//					// .securitySchemes(new ArrayList<ApiKey>(Arrays.asList(new ApiKey("mykey",
//					// "api_key", "header"))))
//					.securitySchemes(new ArrayList<BasicAuth>(Arrays.asList(new BasicAuth("basicAuth"))))
//					.securityContexts(new ArrayList<SecurityContext>(Arrays.asList(securityContext()))).select()
//					.paths(PathSelectors.regex("/v1/.*"))
//					.apis(RequestHandlerSelectors.basePackage("fr.cea.bundle.restapi")).build()
//					.directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
//					.directModelSubstitute(java.time.OffsetDateTime.class, java.util.Date.class);
//		}
//	}
//
//	/**
//	 * Selector for the paths this security context applies to ("/v1/.*")
//	 */
//	private SecurityContext securityContext() {
//		return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/v1/.*"))
//				.build();
//	}
//
//	/**
//	 * Here we use the same key defined in the security scheme (basicAuth)
//	 */
//	List<SecurityReference> defaultAuth() {
//		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
//		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
//		authorizationScopes[0] = authorizationScope;
//		// return new ArrayList<SecurityReference>(Arrays.asList(new
//		// SecurityReference("mykey", authorizationScopes)));
//		return new ArrayList<SecurityReference>(Arrays.asList(new SecurityReference("basicAuth", authorizationScopes)));
//	}
//
//	/**
//	 * it just tells swagger that no special configuration are requested
//	 */
//	@Bean
//	public UiConfiguration uiConfig() {
//		return new UiConfiguration("validatorUrl" // set url
//		);
//	}
//
//	/**
//	 * the metadata are information visualized in the /basepath/swagger-ui.html
//	 * interface, only for documentation
//	 */
//	private ApiInfo metadata() {
//		return new ApiInfoBuilder().title("Bundle Manager API Model with Springboot REST API")
//				.description("CRUD Bundle API Model for CTI management").license("Apache 2.0")
//				.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html").termsOfServiceUrl("").version("1.0.0")
//				.contact(new Contact("Thanh-Hai NGUYEN", "http://www-list.cea.fr/en/", "thanhhai.nguyen@cea.fr"))
//				.build();
//	}

	/**
	 * boot out SpringBoot application
	 */
	public static void main(String[] args) {
		SpringApplication.run(ApplicationDeployer.class, args);
	}

}
